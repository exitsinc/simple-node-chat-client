const http = require("http").createServer();
const io = require("socket.io")(http);
const port = 3000;
const { v4: uuidv4 } = require("uuid");

console.clear();
let users = Object.assign({});
io.on("connection", (socket) => {
  console.log("connected --->  " + socket.id);
  const myUUID = uuidv4();
  const uid = socket.request._query.uid || myUUID;
  console.log("users", users);
  console.log(socket.rooms);
  console.log("====================================");
  console.log({
    reqUid: socket.request._query.uid,
    uid,
    reqEQconst: socket.request._query.uid == uid,
    uuid: myUUID,
    reqEQuuid: uid == myUUID,
  });
  // console.log(io.sockets);
  console.log("====================================");
  // socket.join(uid);
  const sid = socket.id;
  users = Object.assign({}, users, {
    [uid]: sid,
  });

  console.log("usersFINAL", users);

  socket.on("message", (evt) => {
    console.log(evt);
    socket.broadcast.emit("message", evt);
  });
  socket.on("identity", (userId, UserMobile, userName) => {
    console.log("******A CLIENT identity*******", userId, UserMobile, userName);
  });
  socket.on("chat", (friendId, message) => {
    const friendSockId = users[friendId];
    console.log("CHAT", friendId, friendSockId, message);
    socket.to(friendSockId).emit("message", message);
    // console.log("ALL USERS", users);
    // socket.emit("allChattrs", users);
  });
  socket.on("allusers", () => {
    console.log("socket.rooms", io.sockets);
    // console.log("ALL USERS", users);
    // socket.emit("allChattrs", users);
  });
});
io.on("disconnect", (evt) => {
  console.log("disconnected");
});

http.listen(port, () => console.log(`server listening on port: ${port}`));
